import React, {useState, useRef} from 'react';
import {
  Input, Label, Col, InputGroup, FormFeedback
} from 'reactstrap';
import Row from "reactstrap/es/Row";
import InputGroupAddon from "reactstrap/es/InputGroupAddon";

const RULES = {
  email: {
    // Rule Specific
    isValid: (val)=> {
      if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val)) {
        return false;
      } else {
        return true;
      }
    },
    errorMsg: 'Invalid Email Id'
  },
  mobile: {
    isValid: (val)=> {
      if (val.toString().length!== 10) {
        return false;
      } else {
        return true;
      }
    },
    errorMsg: 'Invalid Mobile Number'
  },
} ;

export default function RenderFormField(props) {
  //Store ErrorMessage in state to display
  let [errorMsg, setErrorMessage] = useState('');
  let [touched, setTouched] = useState(false); // wheather user have touched the field or not
  let inputRef = useRef();


  const _handleChange = (event)=> {
    let inputValue = event.target.value;
    setTouched(true);
    //will validate against rule
    let ruleObj = RULES[props.validation];
    let result = {
      name: props.name,
      value: inputValue
    };
    if (ruleObj.isValid(inputValue)) {
      result.valid = true;
      setErrorMessage(''); //clear error message
    }  else {
      result.valid = false;
      setErrorMessage(ruleObj.errorMsg);
    }
    props.handleChange(result);
  };
  return (
    <Row style={{ display: 'flex', marginBottom: '10px', textAlign: 'center'}}>
      <Col sm={props.labelSize} >
        <Label for={props.name} bssize={props.labelSize}>
          {props.label}
          {props.isRequired ? <span className="warning-msg">*</span> : null}
        </Label>
      </Col>
      <Col sm={props.inputSize}>
        <InputGroup>
          {props.addonType ? ( <InputGroupAddon addonType={props.addonType}>{props.addonText}
          </InputGroupAddon>): null}
          <Input
            invalid={errorMsg}
            valid={touched && !errorMsg}
            ref={inputRef}
            type={props.type}
            name={props.name}
            value={props.value}
            bssize={props.inputSize}
            onChange={_handleChange}
          />
          {errorMsg?  <FormFeedback>{errorMsg}</FormFeedback>: ''}
        </InputGroup>
      </Col>
    </Row>
  );
}
