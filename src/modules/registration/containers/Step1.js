import React from 'react'
import Step1Component from '../components/Step1';

function field_required() {

}
function field_validate() {

}
export default class Step1 extends React.Component{
  constructor(props){
    super(props)
    this.state={
      errors:  {
        email: null,
        mobile: null,
      }
    }
  }

  /**
   * if event wont come surbhi will write that she writes code without thinking
   * @param event
   */
  handleChange = (input)=> {
    console.log('asfsdfsdfsdfsdfsdf', input);
    this.props.handleChange(input);
  };

  handleOtpClicked = (event)=> {
    event.preventDefault();
    console.log("register Page otp clicked");
    this.props.otpClicked(event)
  }


  render(){
    return(
      <>
        <Step1Component
          handleChange = {this.handleChange}
          handleOtpClicked = {this.handleOtpClicked}
          email = {this.props.email}
          mobile = {this.props.mobile}
          isClicked={this.props.isClicked}
          errors={this.state.errors}
        />
      </>
    )
  }
}