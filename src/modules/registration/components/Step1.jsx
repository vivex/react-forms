import React from 'react';
import {Button, Card, Col, Form, FormGroup, Label} from "reactstrap";
import OtpInput from "react-otp-input";
import PropTypes from 'prop-types';
import RenderFormField from '../../common/utils/RenderFormField';

const Step1 = (props) => {
  console.log("Supplier registeration", props.msg);
  let emailFieldOptions = {
    name: 'email',
    validation: 'email',
    type: 'email',
    handleChange: props.handleChange,
    value: props.email,
    size: "sm",
    colSize: 12,
    label: 'Email',
    labelSize: '4',
    inputSize: '12',
    isRequired: 'true',
  };

  let mobileFieldOptions = {
    name: 'mobile',
    validation: 'mobile',
    type: 'tel',
    handleChange: props.handleChange,
    value: props.mobile,
    size: "sm",
    colSize: 12,
    label: 'Mobile No',
    labelSize: '4',
    inputSize: '12',
    isRequired: 'true',
    addonType: 'prepand',
    addonText: '+91',
    minLength: 0,
    maxLength: 10,
  };
  return(
    <>
      <div className="register">
        <Card body>
          <Form>
            <RenderFormField {...emailFieldOptions} />
            <RenderFormField {...mobileFieldOptions} />
            {props.isClicked && (
              <>
                <Col sm={8} className="otp-wrapper" style={{
                  paddingBottom: "20px"
                }}>
                  <h5 className="msg-span">Enter One Time Password</h5>
                  <div style={{ marginLeft: "35px", marginBottom: "5px"}}>
                    <OtpInput
                      style={{justifyContent: "center",marginLeft: "25px"}}
                      onChange={otp => console.log(otp)}
                      numInputs={6}
                      separator={<span className="otp-span">-</span>}
                    />
                  </div>
                </Col>
                <h6 className="msg-span">OTP sent to Mobile and Email-id !!!</h6>
              </>
            )}
            <div className={props.isClicked ? "step01-btn-warning-hide": "step01-btn-warning-show"}>
              <FormGroup row>
                <Label for="mobilePhone" sm={2}></Label>
                <Col sm={10}>
                  <div className="warning-msg" style={{paddingLeft: "40px"}}>
                    Note : Please enter valid E-Mail and Mobile No for OTP Verification
                  </div>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={7}>
                  <Button color="primary" className="float-right"
                          disabled={!props.mobile}
                          onClick={props.handleOtpClicked}
                  >Get OTP
                  </Button>
                </Col>
              </FormGroup>
            </div>
          </Form>
        </Card>
      </div>
    </>
  )
}

Step1.propTypes = {
  type: PropTypes.oneOf(['telephone', 'mobile']).isRequired,
  onChange: PropTypes.func,
  renderWarning: PropTypes.func
};

export default Step1;