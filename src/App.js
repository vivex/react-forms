import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as bs from '../node_modules/bootstrap/dist/css/bootstrap.css';


import Step1 from './modules/registration/containers/Step1';


function App() {
  let handleChange = (input)=> {
    console.log('input', input);
  }
  return (
    <div className="App">
      <Step1 handleChange={handleChange}/>
    </div>
  );
}

export default App;
